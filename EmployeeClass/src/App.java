public class App {
    public static void main(String[] args) throws Exception {
        Employee employee1= new Employee(1, "Alex", "Stone", 10000);
        Employee employee2= new Employee(2, "Eva", "Spear", 12000);

        // subtask 3
        System.out.println(employee1);
        System.out.println(employee2);

        // subtask 4
        // System.out.println("Ten day du cua nhan vien 1: " + employee1.getName());
        // System.out.println("Luong 1 nam cua nhan vien 1: " + employee1.getAnnualSalary());
        // System.out.println("Ten day du cua nhan vien 2: " + employee2.getName());
        // System.out.println("Luong 1 nam cua nhan vien 2: " + employee2.getAnnualSalary());

        //subtask 5
        System.out.println("Luong thang cua nhan vien 1 sau khi tang 10%: " + employee1.raiseSalary(10));
        System.out.println("Luong thang cua nhan vien 2 sau khi tang 12%: " + employee2.raiseSalary(12));
    }
}
